import datetime as dt
from random import choices, sample
from typing import Optional, Tuple

from .constants import AVG_N_DAYS_IN_YEAR
from .feature_engineering import compute_daily_cost

import numpy as np
import pandas as pd


def generate_random_meter_df(
    n_meters: int,
    meter_id_range: Tuple[int, int] = (10_000_000, 100_000_000),
    aq_kwh_range: Tuple[int, int] = (10_000, 1_000_000),
    exit_zones: Optional[Tuple[str]] = None,
) -> pd.DataFrame:
    exit_zones = exit_zones if exit_zones is not None else ("EA1", "SO1", "NT1", "SE2")
    data = dict(
        meter_id=sample(range(*meter_id_range), k=n_meters),
        aq_kwh=choices(range(*aq_kwh_range), k=n_meters),
        exit_zone=choices(exit_zones, k=n_meters),
    )
    df = pd.DataFrame(data=data)
    df["aq_kwh"] = df.aq_kwh.astype(float)
    return df


def generate_random_forecast_df(
    n_days: int,
    start_date: dt.date,
    meter_df: pd.DataFrame,
    n_decimals_kwh=2,
) -> pd.DataFrame:
    dfs = []
    for row in meter_df.itertuples():
        total_quantity_kwh = row.aq_kwh / AVG_N_DAYS_IN_YEAR * n_days
        decimal_factor = 10 ** n_decimals_kwh
        len_range = round(decimal_factor * total_quantity_kwh) + 1
        cum_kwh = (
            np.sort([0, len_range - 1] + choices(range(len_range), k=n_days - 1))
            / decimal_factor
        )
        kwh = np.diff(cum_kwh)
        np.testing.assert_almost_equal(
            sum(kwh), total_quantity_kwh, decimal=n_decimals_kwh
        )
        data = dict(
            meter_id=n_days * [row.meter_id],
            date=pd.date_range(start=start_date, periods=n_days, freq="1d"),
            forecast_kwh=np.diff(cum_kwh),
        )
        dfs.append(pd.DataFrame(data=data))
    return pd.concat(dfs, ignore_index=True)


def generate_random_cost_df(
    n_meters: int,
    n_days: int,
    start_date: dt.date,
    rate_df: pd.DataFrame,
) -> pd.DataFrame:
    meter_df = generate_random_meter_df(n_meters)
    forecast_df = generate_random_forecast_df(n_days, start_date, meter_df)
    return compute_daily_cost(meter_df, forecast_df, rate_df)
