import pandas as pd


def compute_daily_cost(
    meter_df: pd.DataFrame,
    forecast_df: pd.DataFrame,
    rate_df: pd.DataFrame,
) -> pd.DataFrame:
    df = forecast_df.merge(meter_df, on=["meter_id"], how="left")
    df = df.rename(columns={"kwh": "forecast_kwh"})
    rate_update_df = rate_df.assign(date=rate_df.last_rate_update_date)[
        ["date", "last_rate_update_date", "exit_zone"]
    ].drop_duplicates()
    df = pd.merge_asof(
        left=df.sort_values(by=["date"]).reset_index(drop=True),
        right=rate_update_df.sort_values(by=["date"]).reset_index(drop=True),
        on="date",
        by=["exit_zone"],
        direction="backward",
        allow_exact_matches=True,
    )
    df = pd.merge_asof(
        left=df.sort_values(by=["aq_kwh"]).reset_index(drop=True),
        right=rate_df.sort_values(by=["aq_min_kwh"]).reset_index(drop=True),
        left_on="aq_kwh",
        right_on="aq_min_kwh",
        by=["exit_zone", "last_rate_update_date"],
        direction="backward",
        allow_exact_matches=True,
    )
    df = df.sort_values(by=["meter_id", "date"])
    df["days_since_last_rate_update"] = (df.date - df.last_rate_update_date).dt.days
    df["days_since_last_forecast"] = df.groupby("meter_id").date.transform(
        lambda x: (x - x.shift(1)).dt.days
    )
    df = df.set_index(["meter_id", "date"], verify_integrity=True).sort_index()
    df["daily_charge_p"] = df.rate_p_per_kwh * df.forecast_kwh
    return df
