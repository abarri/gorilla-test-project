from pathlib import Path
from typing import Dict, Tuple

from .constants import AVG_N_DAYS_IN_YEAR

import pandas as pd


def fetch_meter_df(tsv_path: Path) -> pd.DataFrame:
    df = pd.read_csv(tsv_path, sep="\t")
    df["aq_kwh"] = df.aq_kwh.astype(float)
    return df


def fetch_forecast_df(tsv_path: Path) -> pd.DataFrame:
    df = pd.read_csv(tsv_path, sep="\t")
    df["date"] = pd.to_datetime(df.date)
    return df


def fetch_rate_df(tsv_path: Path) -> pd.DataFrame:
    df = pd.read_csv(tsv_path, sep="\t")
    df["date"] = pd.to_datetime(df.date)
    df["aq_min_kwh"] = df.aq_min_kwh.astype(float)
    df = df.rename(columns={"date": "last_rate_update_date"})
    return df


def match_forecasts_to_annual_quantity(
    meter_df: pd.DataFrame,
    forecast_df: pd.DataFrame,
) -> Tuple[pd.DataFrame, pd.DataFrame]:
    agg_forecast_df = (
        forecast_df.assign(
            sum_forecasts=forecast_df.kwh,
            max_date=forecast_df.date,
            min_date=forecast_df.date,
        )
        .groupby("meter_id")
        .agg(dict(sum_forecasts="sum", max_date="max", min_date="min"))
        .reset_index()
    )
    match_df = meter_df.merge(agg_forecast_df, how="left", on="meter_id")
    match_df["n_days"] = (match_df.max_date - match_df.min_date).dt.days + 1
    match_df["total_kwh_from_aq"] = (
        match_df.aq_kwh * match_df.n_days / AVG_N_DAYS_IN_YEAR
    )
    match_df["correction_factor"] = match_df.total_kwh_from_aq / match_df.sum_forecasts
    match_cols = [
        "meter_id",
        "aq_kwh",
        "n_days",
        "total_kwh_from_aq",
        "sum_forecasts",
        "correction_factor",
    ]
    match_df = match_df[match_cols]
    meter_to_correction_factor = match_df.set_index(
        "meter_id", verify_integrity=True
    ).correction_factor.to_dict()
    matched_forecast_df = forecast_df.copy()
    matched_forecast_df["kwh"] *= forecast_df.meter_id.map(meter_to_correction_factor)
    return match_df, matched_forecast_df
