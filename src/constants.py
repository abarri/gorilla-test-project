from pathlib import Path

PROJECT_PATH = Path(__file__).resolve().parents[1]
DATA_PATH = PROJECT_PATH / "data"

AVG_N_DAYS_IN_YEAR = 365.25
