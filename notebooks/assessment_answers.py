# %% [markdown]
# # Preamble

# %%
import datetime as dt
from functools import partial
from pprint import pprint
from timeit import Timer

import numpy as np
import pandas as pd

import __init__

from src.constants import AVG_N_DAYS_IN_YEAR, DATA_PATH
from src.data_fetching import (
    fetch_forecast_df,
    fetch_meter_df,
    fetch_rate_df,
    match_forecasts_to_annual_quantity,
)
from src.data_generation import (
    generate_random_cost_df,
    generate_random_forecast_df,
    generate_random_meter_df,
)
from src.feature_engineering import compute_daily_cost

# %%
pd.options.display.float_format = "{:.2f}".format

# %% [markdown]
# # Fetch raw data

# %% [markdown]
# ## Meter list

# %%
meter_df = fetch_meter_df(DATA_PATH / "meter_list.tsv")
print(meter_df.dtypes)
display(meter_df)

# %% [markdown]
# ## Consumption data

# %%
raw_forecast_df = fetch_forecast_df(DATA_PATH / "forecast_table.tsv")
display(raw_forecast_df)

# %%
match_df, forecast_df = match_forecasts_to_annual_quantity(meter_df, raw_forecast_df)

# %%
display(match_df)

# %%
print(forecast_df.dtypes)
display(forecast_df)

# %% [markdown]
# ## Rate table

# %%
rate_df = fetch_rate_df(DATA_PATH / "rate_table.tsv")
print(rate_df.dtypes)
display(rate_df)

# %% [markdown]
# # Feature engineering

# %% [markdown]
# ##  Daily cost

# %%
cost_df = compute_daily_cost(forecast_df, meter_df, rate_df)
print(cost_df.dtypes)
display(cost_df)

# %% [markdown]
# ##  Summary

# %%
summary_df = (
    cost_df.assign(
        total_estimated_consumption_kwh=cost_df.forecast_kwh,
        total_cost_per_meter_in_pounds=cost_df.daily_charge_p / 100,
    )
    .reset_index()
    .groupby("meter_id")
    .agg(
        {
            "total_estimated_consumption_kwh": "sum",
            "total_cost_per_meter_in_pounds": "sum",
        }
    )
)

for col in summary_df.columns:
    summary_df[col] = summary_df[col].round(decimals=2)

display(summary_df)

# %% [markdown]
# # Generate random  data

# %% [markdown]
# ## Meter list

# %%
generate_random_meter_df(n_meters=10)

# %%
generate_random_meter_df(
    n_meters=10,
    meter_id_range=(0, 10),
    aq_kwh_range=(0, 200),
    exit_zones=("SO1", "EA1"),
)

# %% [markdown]
# ## Consumption data

# %%
generate_random_forecast_df(
    n_days=3,
    start_date=dt.date(year=2022, month=1, day=1),
    meter_df=generate_random_meter_df(n_meters=3),
)

# %% [markdown]
# ## Daily cost

# %%
generate_random_cost_df(
    n_meters=3,
    n_days=3,
    start_date=dt.date(year=2022, month=1, day=1),
    rate_df=fetch_rate_df(DATA_PATH / "rate_table.tsv"),
)

# %% [markdown]
# # Benchmarks

# %%
rate_df = fetch_rate_df(DATA_PATH / "rate_table.tsv")
n_iterations_per_case = 5
records = []
for n_meters in [10, 100, 1000]:
    for n_days in [10, 100, 1000]:
        partial_generate_random_cost_df = partial(
            generate_random_cost_df,
            n_meters=n_meters,
            n_days=n_days,
            start_date=dt.date(year=2022, month=1, day=1),
            rate_df=rate_df,
        )
        record = dict(
            n_meters=n_meters,
            n_days=n_days,
            seconds=Timer(partial_generate_random_cost_df).timeit(
                n_iterations_per_case
            ),
        )
        records.append(record)
benchmark_df = pd.DataFrame.from_records(records)

# %%
benchmark_df

# %% [markdown]
# # Ideas to improve performance

# %% [markdown]
# Add the rate per kwh to the Consumption table directly after ingestion using e.g. Apache NiFi.

# %% [markdown]
# # Tests

# %% [markdown]
# ## Cost dataframe sanity checks

# %%
assert not cost_df.aq_kwh.isna().any()
assert not cost_df.exit_zone.isna().any()
assert not cost_df.rate_p_per_kwh.isna().any()
assert (cost_df.days_since_last_rate_update <= np.ceil(AVG_N_DAYS_IN_YEAR / 2)).all()
assert (
    cost_df.days_since_last_forecast.isna() | cost_df.days_since_last_forecast == 1
).all()
assert ((cost_df.aq_kwh >= cost_df.aq_min_kwh)).all()
assert ((cost_df.aq_kwh < cost_df.aq_max_kwh) | cost_df.aq_max_kwh.isna()).all()
assert (cost_df.daily_charge_p == cost_df.forecast_kwh * cost_df.rate_p_per_kwh).all()

# %% [markdown]
# ## Annual quantity matches forecasts

# %%
analysis_df = cost_df.copy().reset_index()
analysis_df = (
    analysis_df.assign(
        n_days=analysis_df.date,
        min_aq_kwh=analysis_df.aq_kwh,
        max_aq_kwh=analysis_df.aq_kwh,
        sum_forecast_kwh=analysis_df.forecast_kwh,
    )
    .groupby("meter_id")
    .agg(
        dict(n_days="count", min_aq_kwh="min", max_aq_kwh="max", sum_forecast_kwh="sum")
    )
)
analysis_df["aq_kwh"] = analysis_df.min_aq_kwh
analysis_df["total_kwh_from_aq"] = (
    analysis_df.aq_kwh / AVG_N_DAYS_IN_YEAR * analysis_df.n_days
)
display(analysis_df[["n_days", "aq_kwh", "total_kwh_from_aq", "sum_forecast_kwh"]])
assert (analysis_df.min_aq_kwh == analysis_df.min_aq_kwh).all()
assert (analysis_df.total_kwh_from_aq == analysis_df.sum_forecast_kwh).all()
